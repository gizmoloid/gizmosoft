import PEngine from "./js/physics/PEngine.js";

const physics = new PEngine(() => {
    self.postMessage({
        data: [2, 3]
    })
})

self.addEventListener("message", (e) => {

    switch(e.data.action) {
        case "UPDATE":
        console.log("update physics");
        break;
        case "RUN":
        physics.run();
        break;
        case "PAUSE":
        physics.pause();
        break;
    }   
});