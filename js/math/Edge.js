class Edge {

    constructor(gradients, minYVert, maxYVert, minYVertIndex) {

        this.yStart = parseInt(Math.ceil(minYVert.pos.y));
        this.yEnd = parseInt(Math.ceil(maxYVert.pos.y));

        const yDist = maxYVert.pos.y - minYVert.pos.y;
        const xDist = maxYVert.pos.x - minYVert.pos.x;

        const yPrestep = this.yStart - minYVert.pos.y;
        this.xStep = xDist / yDist;
        this.x = minYVert.pos.x + yPrestep * this.xStep;
        const xPrestep = this.x - minYVert.pos.x;

        this.texCoordX = gradients.getTexCoordX(minYVertIndex) + gradients.texCoordXXStep * xPrestep + gradients.texCoordXYStep * yPrestep;
		this.texCoordXStep = gradients.texCoordXYStep + gradients.texCoordXXStep * this.xStep;

        this.texCoordY = gradients.getTexCoordY(minYVertIndex) + gradients.texCoordYXStep * xPrestep + gradients.texCoordYYStep * yPrestep;
		this.texCoordYStep = gradients.texCoordYYStep + gradients.texCoordYXStep * this.xStep;

        this.oneOverZ = gradients.getOneOverZ(minYVertIndex) + gradients.oneOverZXStep * xPrestep + gradients.oneOverZYStep * yPrestep;
		this.oneOverZStep = gradients.oneOverZYStep + gradients.oneOverZXStep * this.xStep;

        this.depth = gradients.getDepth(minYVertIndex) + gradients.depthXStep * xPrestep + gradients.depthYStep * yPrestep;
        this.depthStep = gradients.depthYStep + gradients.depthXStep * this.xStep;

        this.lightAmt = gradients.getLightAmt(minYVertIndex) + gradients.lightAmtXStep * xPrestep + gradients.lightAmtYStep * yPrestep;
        this.lightAmtStep = gradients.lightAmtYStep + gradients.lightAmtXStep * this.xStep;
    }

    step() {
        this.x += this.xStep;
		this.texCoordX += this.texCoordXStep;
		this.texCoordY += this.texCoordYStep;
		this.oneOverZ += this.oneOverZStep;
		this.depth += this.depthStep;
		this.lightAmt += this.lightAmtStep;
    }

    // getYStart() { return this.yStart; }
    // getYEnd() { return this.yEnd };
    // getX() { return this.x };
	// getTexCoordX() { return this.texCoordX; }
	// getTexCoordY() { return this.texCoordY; }
	// getOneOverZ() { return this.oneOverZ; }
    // getDepth() {return this.depth; }
    // getLightAmt() {return this.lightAmt; }
}

export default Edge;