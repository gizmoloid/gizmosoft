class Vec4 {

    constructor(x=0, y=0, z=0, w=0) {

        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    length() {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w);
    }

    equals(vec) {

        return  this.x === vec.x &&
                this.y === vec.y &&
                this.z === vec.z
    }

    static greaterOrEqualThan(vecA, vecB) {
        return vecA.x >= vecB.x && vecA.y >= vecB.y && vecA.z >= vecB.z;
    }

    max() {
        return Math.max(Math.max(this.x, this.y), Math.max(this.z, this.w));
    }

    dot(r) {
        return this.x * r.x + this.y * r.y + this.z * r.z + this.w * r.w;
    }

    cross(vec) {
        return new Vec4(
            this.y * vec.z - this.z * vec.y,
            this.z * vec.x - this.x * vec.z,
            this.x * vec.y - this.y * vec.x, 0);
    }

    clone() {
        return new Vec4(this.x, this.y, this.z, this.w);
    }

    static crossVecs(a, b){
        
        return new Vec4(
            a.y * b.z - a.z * b.y,
            a.z * b.x - a.x * b.z,
            a.x * b.y - a.y * b.x, 0);
    }

    /**
     * @param {Vec4} from 
     * @param {Vec4} to 
     * @param {Number} delta 
     * @returns {Vec4}
     */
    static lerp(from, to, delta) {
        if(delta > 1) {
            return to;
        } else {
            return to.subtractVec(from).multiply(delta).addVec(from);
        }
    }

    // Vec4, float / returns: Vec4
    // lerp(dest, lerpFactor) {
    //     if(lerpFactor > 1) {
    //         return dest;
    //     } else {
    //         return dest.subtractVec(this).multiply(lerpFactor).addVec(this);
    //     }
    // }
    
    

    /**
     * @returns Vec4
     */
    normalized() {

        if(this.length() === 0) return this;
        const length = this.length();
		return new Vec4(this.x / length, this.y / length, this.z / length, this.w / length);
    }

	rotate(axis, angle) {
		const cosAngle = Math.cos(-angle);
		return this.cross(axis.mul(Math.sin(-angle))).add(              //Rotation on local X
				(this.mul(cosAngle)).add(                       //Rotation on local Z
                axis.mul(this.dot(axis.mul(1 - cosAngle)))));   //Rotation on local Y
	}

    //Quaternion
    rotateQuat(rotation) {

		const w = rotation.multiplyVec(this).multiplyQuat(rotation.conjugate());
		return new Vec4(w.getX(), w.getY(), w.getZ(), 1.0);
    }

    add(r) {
        return new Vec4(this.x + r, this.y + r, this.z + r, this.w + r);
    }

    addVec(r) {
        return new Vec4(this.x + r.x, this.y + r.y, this.z + r.z, this.w + r.w);
    }

    addScaledVector(vector, scale) {

        this.x += vector.x * scale;
        this.y += vector.y * scale;
        this.z += vector.z * scale;
    }
    plusEquals(vector) {
        this.x += vector.x;
        this.y += vector.y;
        this.z += vector.z;
    }

    timesEqualsNum(value) {

        this.x *= value;
        this.y *= value;
        this.z *= value;
    }
    clear() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    subtract(r) {
        return new Vec4(this.x - r, this.y - r, this.z - r, this.w - r);
    }

    subtractVec(r) {
        return new Vec4(this.x - r.x, this.y - r.y, this.z - r.z, this.w - r.w);
    }

    subtractQuat(r) {
        return new Vec4(this.x - r.x, this.y - r.y, this.z - r.z, this.w - r.w);
    }

    multiply(r) {
        return new Vec4(this.x * r, this.y * r, this.z * r, this.w * r);
    }

    multiplyVec(r) {
        return new Vec4(this.x * r.x, this.y * r.y, this.z * r.z, this.w * r.w);
    }

    divide(r) {
        return new Vec4(this.x / r, this.y / r, this.z / r, this.w / r);
    }

    getX() {return this.x}
    getY() {return this.y}
    getZ() {return this.z}
    getW() {return this.w}

    //static
    static up() {
        return new Vec4(0,1,0);
    }
    static right() {
        return new Vec4(1,0,0);
    }
    static forward() {
        return new Vec4(0,0,1);
    }
    static all() {
        return new Vec4(1,1,1);
    }

    static addVecs(vecA, vecB) {

        return new Vec4(vecA.x + vecB.x,
                        vecA.y + vecB.y,
                        vecA.z + vecB.z,
                        vecA.w + vecB.w);
    }

    static subtractVecs(vecA, vecB) {

        return new Vec4(vecA.x - vecB.x,
                        vecA.y - vecB.y,
                        vecA.z - vecB.z,
                        vecA.w - vecB.w)
    }

    static multiplyVecByNum(vec, n) {

        return new Vec4(vec.x * n,
                        vec.y * n,
                        vec.z * n,
                        vec.w * n);
    }
                    
    //params: Vec4,Vec4,  float / returns: Vec4
    static lerpVecs(startP, endP, percent) {
        if(percent >= 1) {
            return endP;
        }
        return Vec4.subtractVecs(endP, startP).multiply(percent).addVec(startP);
    }


    static nLerp(start, end, percent) {
        if(percent >= 1) {
            return endP;
        }
        return Vec4.lerpVecs(start,end,percent).normalized();
    }

    //params: Vec4, Vec4 / returns: Vec4
    static distance(vecA, vecB) {
        return Math.sqrt(Math.pow(vecA.x - vecB.x, 2) + Math.pow(vecA.y - vecB.y, 2) + Math.pow(vecA.z - vecB.z, 2));
    }
}

export default Vec4;