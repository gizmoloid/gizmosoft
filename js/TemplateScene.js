import Scene from "./core/Scene.js";
import Vec4 from "./math/Vec4.js";
import Input from "./core/Input.js";
import Car from "./components/Car.js";
import Terrain from "./terrains/Terrain.js";
import Camera from "./components/Camera.js";

class TemplateScene extends Scene {

    constructor(display, resources) {
        super(display, resources);

        this.input = new Input();

        this.road = resources.getObject3D("road");
        // this.addObject3D(this.road);

        this.car = new Car(resources);
        this.addObject3D(this.car);

        // this.terrain = new Terrain(
        //     0, 0,
        //     resources.getTexture(2).clone(),
        //     resources.getTexture(3).clone().bmp
        // );
        // this.addObject3D(this.terrain.model);

        this.quad = resources.getObject3D("terrain");
        this.addObject3D(this.quad);

        this.camera.setPositionOffset(new Vec4(0,0.5,-1));
    }

    update(timeline) {

        // this.reset();

        this.car.updateInput(this.input, timeline.delta);

        // this.quad.transform.position = new Vec4(Math.sin(timeline.tick), 0, 0);
        this.camera.follow(this.car, timeline.delta);
        // this.camera.lookAt(this.terrain.model.transform.position, Camera.Y_AXIS);

        super.update(timeline);
    }

    reset() {
        // super.reset();
        // this.camera.reset();
    }
}

export default TemplateScene;