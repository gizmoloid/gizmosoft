import Bitmap from "./core/Bitmap.js";
import Texture from "./components/Texture.js";
import OBJModel from "./components/OBJModel.js";
import Mesh from "./components/Mesh.js";
import Object3D from "./components/Object3D.js";

class Resources {

    constructor(callback) {

        this.root = "resources/";
        this.callback = callback;

        //Bitmap[]
        this.atlasMap = null;
        this.textures = [];
        this.texturesLoaded = false;
        this.audios = [];
        this.audiosLoaded = false;
        this.audioContext = new(window.AudioContext || window.webkitAudioContext)
        this.font = [];

        //Mesh[]
        this.meshes = [];
        this.objects3D = [];
        this.meshesLoaded = false;

        fetch("./resources/resources.json").
        then(res => {
            return res.json()
        }).
        then(jRes => {
            this.init(jRes);
        });
    }

    init(data) {

        this.createTextures(data.atlas, data.textures, () => {
            this.createAudios(data.audios, () => {
                this.createObjects(data.objects);
            });
        });

        //props;
        this.props = data.props;
        this.props2 = data.props2;
    }

    createTextures(atlas, data, cb) {

        this.callback(Resources.state.LOADING_TEXTURES);

        const instance = this;

        const image = new Image();
        image.onload = function() {

            instance.atlasMap = this;
            
            const canvas = document.createElement("canvas");
            canvas.width = atlas.w;
            canvas.height = atlas.h;
            const ctx = canvas.getContext("2d");
            ctx.drawImage(this, 0, 0);

            let d;
            let bmps;
            let srcData;
            let copiedData;
            let sliceX;
            let sliceY;
            let sliceW;
            let sliceH;

            for(let i = 0; i < data.length; i++) {

                bmps = [];
                d = data[i];
                if(d.spritesH) {
                    const sw = d.w / d.spritesH;
                    const sh = d.h / d.spritesV

                    const numSprites = d.spritesH * d.spritesV;
                    let row = 0;
                    let col = 0;
                    for(let j = 0; j < numSprites; j++) {

                        sliceX  = d.x + (sw * col);
                        sliceY  = d.y + (sh * row);
                        sliceW  = sw;
                        sliceH  = sh;

                        srcData = ctx.getImageData(sliceX, sliceY, sliceW, sliceH);

                        copiedData = new Uint8ClampedArray(srcData.data.length);
                        copiedData.set(srcData.data);
                        bmps.push(new Bitmap( {
                            w: sliceW,
                            h: sliceH,
                            imgData: copiedData,
                            rad: 1,
                            name: d.name,
                            mode: parseInt(d.mode)
                        }));
                        
                        col++;
                        if(col >= d.spritesH) {
                            col = 0;
                            row++;
                        }
                    }
                } else {

                    sliceX  = d.x;
                    sliceY  = d.y;
                    sliceW  = d.w;
                    sliceH  = d.h;
                    srcData = ctx.getImageData(sliceX, sliceY, sliceW, sliceH);
                    copiedData = new Uint8ClampedArray(srcData.data.length);
                    copiedData.set(srcData.data);
                    bmps.push(new Bitmap( {
                        w: sliceW, 
                        h: sliceH,
                        imgData: copiedData,
                        rad: 1,
                        name: d.name,
                        mode: parseInt(d.mode)
                    }));
                }
                
                instance.textures.push(new Texture({
                    id: d.id,
                    bmps: bmps,
                    type: d.type
                }));
            }

            instance.texturesLoaded = true;
            instance.checkLoadingComplete();

            cb();
        }
        image.src = this.root + atlas.url;
    }

    createAudios(data, cb) {

        if(data.length === 0) {
            this.audiosLoaded = true;
            this.checkLoadingComplete();
            cb();
            return;
        }

        const totalAudios = data.length;
        this.callback(Resources.state.LOADING_AUDIO,  " (1/" + totalAudios + ")");
        let counter = 0;
        const instance = this;
        for (let i = 0; i < data.length; i++) {
            Utils.loadFile(data[i].id, this.root + data[i].url, (loadedData, id) => {
                instance.audioContext.decodeAudioData(loadedData, (buffer) => {
                    instance.audios.push(new AudioSource(id, instance.audioContext, buffer));
                    counter++;
                    instance.callback(Resources.state.LOADING_AUDIO, " (" + (counter + 1) + "/" + totalAudios + ")");
                    if(counter === totalAudios) {
                        instance.audios.sort((a, b) => {
                            return a.id-b.id;
                        });
                        instance.audiosLoaded = true;
                        instance.checkLoadingComplete();
                        cb();
                    }
                });
            }, "arraybuffer");
        }
    }

    createObjects(data) {

        this.callback(Resources.state.LOADING_MESHES);
        const totalMeshes = data.length;
        let counter = 0;

        for(let i = 0; i < data.length; i++) {
            const oData = data[i];

            const callback = (cdata) => {

                const obj3D = new Object3D({
                    mesh: null,
                    rad: 0,
                    name: oData.name,
                    texture: this.getTexture(oData.texture)
                })
                for(let i= 0; i < cdata.length; i++) {
                    obj3D.addObjects(new Object3D({
                        mesh: new Mesh(cdata[i]),
                        rad: 0,
                        name: cdata[i].name,
                        texture: this.getTexture(oData.texture)
                    }));
                }

                this.objects3D.push(obj3D);

                counter++;
                if(counter === totalMeshes) {
                    this.meshesLoaded = true;
                    this.checkLoadingComplete();
                }
            }

            switch(data[i].type) {
                case Resources.fileType.FBX:
                    FBXLoader.loadFile(this.root + data[i].url, callback);
                    break;
                case Resources.fileType.OBJ:
                    new OBJModel().loadFile(data[i].name + i.toString(), this.root + data[i].url, callback);
                    break;
            }
        }
    }

    checkLoadingComplete() {

        if( this.texturesLoaded && this.audiosLoaded && this.meshesLoaded ) {
            this.callback(Resources.state.LOADING_COMPLETE);
        }
    }

    //GETTERS SETTERS
    getKeyframes() {
        return this.keyframes;
    }
    getData() {
        return {
            textures: this.textures,
            audios: this.audios,
            meshes: this.meshes,
            keyframes: this.keyframes
        }
    }
    getTexture(id) {

        for(var i = 0; i < this.textures.length; i++) {
            if(this.textures[i].id == id) {
                return this.textures[i].clone();
            }
        }
        return null;
    }
    getObject3D(name) {

        for (let i = 0; i < this.objects3D.length; i++) {
            if(this.objects3D[i].name === name) return this.objects3D[i].clone();
        }
    }
    getMesh(name) {
        for(let i = 0; i < this.meshes.length; i++) {
            if(this.meshes[i].name == name) {
                return this.meshes[i].clone();
            }
        }
        return null;
    }
    getAudio(id) {

        for(var i = 0; i < this.audios.length; i++) {

            if(this.audios[i].id == id) {
                return this.audios[i];
            }
        }
        return null;
    }
    getAudioContext(id) {

        return this.audioContext;
    }
    getAudios() {
        return this.audios;
    }
    getTag() {
        return this.tagAscii;
    }
}

Resources.state = {
    LOADING_TEXTURES: "loading textures",
    LOADING_AUDIO: "loading audio",
    LOADING_MESHES: "loading meshes",
    LOADING_FONT: "loading font",
    LOADING_COMPLETE: "loading complete"
}

Resources.fileType = {
    FBX: "fbx",
    OBJ: "obj",
    JPG: "jpg",
    PNG: "png",
    MP3: "mp3",
    OGG: "ogg"
}

export default Resources;