class Input {

    constructor() {
        this.currentKeyDown = null;

        this.keys = new Array(65536);
        for (let i = 0; i < 65536; i++) {
            this.keys[i] = false;
        }
        document.addEventListener("keydown", (e) => {
            this.keys[e.keyCode] = true;
        });
        document.addEventListener("keyup", (e) => {
            this.keys[e.keyCode] = false;
        });

        //SETUP CONTROLS EVENTS
        up.addEventListener("touchstart", () => this.keys[Input.Key.W] = true, false );
        up.addEventListener("touchend", () => this.keys[Input.Key.W] = false, false );

        down.addEventListener("touchstart", () => this.keys[Input.Key.S] = true, false );
        down.addEventListener("touchend", () => this.keys[Input.Key.S] = false, false );


        left.addEventListener("touchstart", () => this.keys[Input.Key.LEFT] = true, false );
        left.addEventListener("touchend", () => this.keys[Input.Key.LEFT] = false, false );

        right.addEventListener("touchstart", () => this.keys[Input.Key.RIGHT] = true, false );
        right.addEventListener("touchend", () => this.keys[Input.Key.RIGHT] = false, false );
    }

    getKey(key) {
        return this.keys[key];
    }
}

Input.Key = {
    W: 87,
    A: 65,
    S: 83,
    D: 68,
    UP: 38,
    DOWN: 40,
    LEFT: 37,
    RIGHT: 39,
    ESC: 27
}

export default Input;