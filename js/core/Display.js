import RenderContext from "./RenderContext.js";

class Display {
    constructor(data) {
        this.reset(data);
    }

    swapBuffers() {
        this.displayImage.data.set(this.frameBuffer.components);
        this.context.putImageData(this.displayImage, 0, 0);
    }

    reset(data) {

        this.canvas = null;
        this.context = null;
        this.frameBuffer = null;

        this.canvas = document.createElement("canvas");
        this.canvas.id = "canvas";
        this.canvas.width = data.w;
        this.canvas.height = data.h;
        
        document.body.prepend(this.canvas);

        this.context = this.canvas.getContext("2d");
        this.context.imageSmoothingEnabled = false;
        this.context.font = "10px Arial";
        this.frameBuffer = new RenderContext(this.canvas.width, this.canvas.height);
        this.frameBuffer.clear(0X00);

        this.displayImage = new ImageData(this.canvas.width, this.canvas.height);
    }

    getCanvas() {
        return this.canvas;
    }

    paintFPS(fps) {
        this.context.fillStyle = "white";
        this.context.fillText(fps.toString(),2,10);
    }

    clear() {
        this.frameBuffer.clear(0x00);
        this.frameBuffer.clearDepthBuffer();
    }

}

Display.Options = {

    SHOW_HIDE_WIREFRAME: 0
}

Display.Quality = [
    {
        w: 240,
        h: 180,
        className: "shit"
    },
    {
        w: 480,
        h: 320,
        className: "fair"
    },
    {
        w: 800,
        h: 600,
        className: "cool"
    }
];

export default Display;