import Bitmap from "./Bitmap.js";
import Mat4 from "../math/Mat4.js";
import Gradients from "../math/Gradients.js";
import Edge from "../math/Edge.js";
import DirectionalLight from "../components/DirectionalLight.js";

class RenderContext extends Bitmap {

    constructor(w, h) {
        super({
            w: w,
            h: h
        });

        this.zBuffer = [];
        this.zBuffer.length = this.width * this.height;

        this.showEdges = false;
        this.vertices = [];

        this.light = new DirectionalLight();
    }

    clearDepthBuffer() {
        for(var i = 0; i < this.zBuffer.length; i++) {
            this.zBuffer[i] = Number.MAX_VALUE;
        }
    }

    //FIX THIS SHIT...
    drawLine(x1, y1, x2, y2, col=Color.white) {
        // delta of exact value and rounded value of the dependant variable
        let d = 0;

        const dy = Math.abs(y2 - y1);
        const dx = Math.abs(x2 - x1);
 
        const dy2 = (dy << 1); // slope scaling factors to avoid floating
        const dx2 = (dx << 1); // point
 
        const ix = x1 < x2 ? 1 : -1; // increment direction
        const iy = y1 < y2 ? 1 : -1;
        if (dy <= dx) {
            for (let k = 0; k < dx; k++) {
                this.drawPixel(parseInt(x1), parseInt(y1), col.r, col.g, col.b, col.a);
        
                if (x1 == x2)
                    break;
                x1 += ix;
                d += dy2;
                if (d > dx) {
                    y1 += iy;
                    d -= dx2;
                }
            }
        } else {
            for (let k = 0; k < dx; k++) {
                this.drawPixel(parseInt(x1), parseInt(y1), col.r, col.g, col.b, col.a);
                if (y1 == y2)
                    break;
                y1 += iy;
                d += dx2;
                if (d > dy) {
                    x1 += ix;
                    d -= dy2;
                }
            }
        }
    }
    drawCurve(points, col=Color.white) {

        let pA;
        let pB;

        const screenSpaceTransform = new Mat4().screenSpaceTransform(this.width / 2, this.height / 2);
        let identity;

        for(let i = 0; i < points.length - 1; i++ ) {

            identity = new Mat4().identity();
            pA = points[i].transform(screenSpaceTransform, identity).perspectiveDivide();
            pB = points[i + 1].transform(screenSpaceTransform, identity).perspectiveDivide();
            this.drawLine(pA.pos.x, pA.pos.y, pB.pos.x, pB.pos.y);
        }
    }
    drawTangents(points, col=Color.white) {

        var pA;
        var pB;
        var screenSpaceTransform = new Mat4().screenSpaceTransform(this.width / 2, this.height / 2);

        for(var i = 0; i < points.length; i++) {

            var identity = new Mat4().identity();
            pA = points[i].a.transform(screenSpaceTransform, identity).perspectiveDivide();
            pB = points[i].b.transform(screenSpaceTransform, identity).perspectiveDivide();
            this.drawLine(pA.pos.x, pA.pos.y, pB.pos.x, pB.pos.y, col);
        }
    }

    //Vertex, Vertex, Vertex, Bitmap
    drawTriangle(v1, v2, v3, texture) {

        if( v1.isInsideViewFrustum() && 
            v2.isInsideViewFrustum() && 
            v3.isInsideViewFrustum() ) {
            this.fillTriangle(v1, v2, v3, texture);
            return;
        }
        this.vertices = [v1, v2, v3];
        if( this.clipPolygonAxis(0) &&
            this.clipPolygonAxis(1) &&
            this.clipPolygonAxis(2)) {

                for(let i = 1; i < this.vertices.length - 1; i++) {
                    this.fillTriangle(this.vertices[0], this.vertices[i], this.vertices[i + 1], texture)
                }
        }
    }

    //Vertex[], Vertex[], int
    clipPolygonAxis(componentindex) {

        let auxillaryList = [];

        this.clipPolygonComponent(this.vertices, componentindex, 1.0, auxillaryList);
        this.vertices = [];

        if(auxillaryList.length === 0) {
            return false;
        }
        this.clipPolygonComponent(auxillaryList, componentindex, -1.0, this.vertices);

        return this.vertices.length > 0;
    }
    //Vertex[], int, float, Vertex[]
    clipPolygonComponent(vertices, componentIndex, componentFactor, result) {

        let previousVertex = vertices[vertices.length - 1];
        let previousComponent = previousVertex.get(componentIndex) * componentFactor;
        let previousInside = previousComponent <= previousVertex.pos.w;

        for (let i = 0; i < vertices.length; i++) {
            const currentVertex = vertices[i];
            const currentComponent = currentVertex.get(componentIndex) * componentFactor;
			const currentInside = currentComponent <= currentVertex.pos.w;

            if(currentInside ^ previousInside) {
                result.push(previousVertex.lerp(currentVertex, 
                    (previousVertex.pos.w - previousComponent) /
                    ((previousVertex.pos.w - previousComponent) - 
                    (currentVertex.pos.w - currentComponent))));
			}

            if(currentInside) {
				result.push(currentVertex);
			}

			previousVertex = currentVertex;
			previousComponent = currentComponent;
			previousInside = currentInside;
            
        }

    }

    fillTriangle(v1, v2, v3, texture) {

        const screenSpaceTransform = new Mat4().screenSpaceTransform(this.width / 2, this.height / 2);
        const identity = new Mat4().identity();

        let minYVert = v1.transform(screenSpaceTransform, identity).perspectiveDivide();
        let midYVert = v2.transform(screenSpaceTransform, identity).perspectiveDivide();
        let maxYVert = v3.transform(screenSpaceTransform, identity).perspectiveDivide();

        if(minYVert.triangleAreaTimesTwo(maxYVert, midYVert) >= 0) {
            return;
        }

        if(maxYVert.pos.y < midYVert.pos.y) {
            let tmp = maxYVert;
            maxYVert = midYVert;
            midYVert = tmp;
        }
        if(midYVert.pos.y < minYVert.pos.y) {
            let tmp = midYVert;
            midYVert = minYVert;
            minYVert = tmp;
        }
        if(maxYVert.pos.y < midYVert.pos.y) {
            let tmp = maxYVert;
            maxYVert = midYVert;
            midYVert = tmp;
        }

        this.scanTriangle(minYVert, midYVert, maxYVert, minYVert.triangleAreaTimesTwo(maxYVert, midYVert) >= 0, texture);
    }

    scanTriangle(minYVert, midYVert, maxYVert, handedness, texture) {

        const gradients = new Gradients(minYVert, midYVert, maxYVert, this.light.dir);

        const topToBottom    = new Edge(gradients, minYVert, maxYVert, 0);

        this.scanEdges(gradients, topToBottom, new Edge(gradients, minYVert, midYVert, 0), handedness, texture);
        this.scanEdges(gradients, topToBottom, new Edge(gradients, midYVert, maxYVert, 1), handedness, texture);
    }

    scanEdges(gradients, a, b, handedness, texture) {

        let left = a;
        let right = b;

        if(handedness) {
            const tmp = left;
            left = right;
            right = tmp;
        }

        for(let j = b.yStart; j < b.yEnd; j++) {
            this.drawScanLine(gradients, left, right, j, texture);
            left.step();
            right.step();
        }
    }

    drawScanLine(gradients, left, right, j, texture) {
        const xMin = Math.ceil(left.x);
        const xMax = Math.ceil(right.x);
        const xPrestep = xMin - left.x;

        const xDist = right.x - left.x;
		const texCoordsXXStep = (right.texCoordX - left.texCoordX) / xDist;
		const texCoordsYXStep = (right.texCoordY - left.texCoordY) / xDist;
		const oneOverZXStep = (right.oneOverZ - left.oneOverZ) / xDist;
		const depthXStep = (right.depth - left.depth) / xDist;
        const lightAmtXStep = gradients.lightAmtXStep;

        let texCoordX = left.texCoordX + texCoordsXXStep * xPrestep;
        let texCoordY = left.texCoordY + texCoordsYXStep * xPrestep;
        let oneOverZ = left.oneOverZ + oneOverZXStep * xPrestep;
        let depth = left.depth + depthXStep * xPrestep;
        let lightAmt = left.lightAmt + lightAmtXStep * xPrestep;
        
        for(let i = xMin; i < xMax; i++) {

            const index = i + j * this.width
            if(depth < this.zBuffer[index]) {
                
                this.zBuffer[index] = depth;
                const z = 1.0 / oneOverZ;
                const srcX = Math.ceil( ((texCoordX * z) * (texture.width - 1) + 0.5) );
                const srcY = Math.ceil( ((texCoordY * z) * (texture.height - 1) + 0.5) );

                this.copyPixel(i, j, srcX, srcY, texture, lightAmt * this.light.intensity);
                // if(texture.useLight) {
                    
                // } else {
                //     this.copyPixel(i, j, srcX, srcY, texture, 1);
                // }
            }
            
            oneOverZ += oneOverZXStep;
            texCoordX += texCoordsXXStep;
            texCoordY += texCoordsYXStep;
            depth += depthXStep;
            lightAmt += lightAmtXStep;
        }
    }
}

export default RenderContext;