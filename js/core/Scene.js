import Camera from "../components/Camera.js";
import Vec4 from "../math/Vec4.js";

class Scene {
    constructor(display, resources) {

        this.renderer = display.frameBuffer;
        this.camera = new Camera(display.frameBuffer);
        this.resources = resources;
        this.objects3D = [];
        this.numObjs3D = 0;
    }

    update(timeline) {

        // this.camera.update(tick);

        for(let i = 0; i < this.numObjs3D; i++) {
            this.objects3D[i].update(this.renderer, this.camera.getViewProjection());
        }
    }
    reset() {
        for(let i = 0; i < this.numObjs3D; i++) {
            this.objects3D[i].reset();
        }
    }
    addObject3D(object3d) {
        this.objects3D.push(object3d);
        this.numObjs3D = this.objects3D.length;
    }
}

export default Scene;