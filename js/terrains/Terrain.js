import Object3D from "../components/Object3D.js";
import OBJModel from "../components/OBJModel.js";
import Mesh from "../components/Mesh.js";
import Vec4 from "../math/Vec4.js";
class Terrain {
    /**
     * 
     * @param {Number} gridX 
     * @param {Number} gridZ 
     */
    constructor(gridX, gridZ, texture, heightMap) {

        this.x = gridX * Terrain.SIZE;
        this.y = gridZ * Terrain.SIZE;

        this.model = new Object3D({
            mesh: new Mesh(new OBJModel().generate(this.generateTerrain(heightMap))),
            rad: 0,
            name: "terrain",
            texture: texture
        });
    }


    generateTerrain(heightMap){

        // console.log(heightMap);
        const VERTEX_COUNT = heightMap.height;
        const count = VERTEX_COUNT * VERTEX_COUNT;
        const vertices = new Array(count * 3);
        const normals = new Array(count * 3);
        const textureCoords = new Array(count * 2);
        const indices = new Array(6 * (VERTEX_COUNT - 1) * (VERTEX_COUNT - 1));
        let vertexPointer = 0;
        for(let i = 0;i < VERTEX_COUNT; i++) {
            for(let j = 0; j < VERTEX_COUNT; j++) {

                
                vertices[vertexPointer * 3] = j / (VERTEX_COUNT - 1) * Terrain.SIZE;
                vertices[vertexPointer * 3 + 1] = this.getHeight(j, i, heightMap);
                vertices[vertexPointer * 3 + 2] = i / (VERTEX_COUNT - 1) * Terrain.SIZE;
                const normal = this.calcNormal(j, i, heightMap);
                normals[vertexPointer * 3] = normal.x;
                normals[vertexPointer * 3 + 1] = normal.y;
                normals[vertexPointer * 3 + 2] = normal.z;
                textureCoords[vertexPointer * 2] = j / (VERTEX_COUNT - 1);
                textureCoords[vertexPointer * 2 + 1] = i / (VERTEX_COUNT - 1);
                vertexPointer++;
            }
        }
        let pointer = 0;
        for(let i = 0; i < VERTEX_COUNT - 1; i++) {
            for(let j = 0; j < VERTEX_COUNT - 1; j++){
                const topLeft = ( i * VERTEX_COUNT)+j;
                const topRight = topLeft + 1;
                const bottomLeft = ((i + 1) * VERTEX_COUNT) + j;
                const bottomRight = bottomLeft + 1;
                indices[pointer++] = topLeft;
                indices[pointer++] = bottomLeft;
                indices[pointer++] = topRight;
                indices[pointer++] = topRight;
                indices[pointer++] = bottomLeft;
                indices[pointer++] = bottomRight;
            }
        }

        return {
            vertices: vertices,
            normals: normals,
            texCoords: textureCoords,
            indices: indices,
            name: "terrain"
        }
    }

    calcNormal(x, z, hMap) {
        const hL = this.getHeight(x-1, z, hMap);
        const hR = this.getHeight(x+1, z, hMap);
        const hD = this.getHeight(x, z-1, hMap);
        const hU = this.getHeight(x, z+1, hMap);

        const n = new Vec4(hL-hR, 2, hD-hU)
        return n.normalized();

    }
    getHeight(x, z, hMap) {

        if(x<0 || x>=hMap.height || z<0 || z>=hMap.height) return 0;

        let h = hMap.getRGB(x, z);
        // h += Terrain.MAX_PIXEL_COLOR / 2;
        // h /= Terrain.MAX_PIXEL_COLOR / 2;
        // h *= Terrain.MAX_HEIGHT;

        // console.log(h);

        return h / Terrain.MAX_HEIGHT;
    }
}

Terrain.SIZE = 80;
Terrain.MAX_HEIGHT = 40;
Terrain.MAX_PIXEL_COLOR = 256 * 256 * 256;

export default Terrain;