import Timeline from "./core/Timeline.js";
import Display from "./core/Display.js";
import Resources from "./Resources.js";
import TemplateScene from "./TemplateScene.js";

class GizmoSoft {
    constructor(callback) {
        this.callback = callback;
        this.timeline = new Timeline({
            keyframes: [],
            callback: () => { }
        });
        this.display = new Display({
            w: GizmoSoft.CANVAS_SIZE.w,
            h: GizmoSoft.CANVAS_SIZE.h
        });
        this.resources = new Resources((message) => {
            if (message === Resources.state.LOADING_COMPLETE) {
                this.scene = new TemplateScene(this.display, this.resources);
                this.callback(GizmoSoft.STATUS.READY);
            }
        });
    }

    run() {

        this.isRunning = true;

        let fps = 0;
        let frames = 0;
        let frameCounter = 0;
        const frameRate = 1.0 / GizmoSoft.FPS;

        let lastTime = Timeline.time();
        let unprocessedTime = 0;

        let loop = (e) => {

            let render = false;

            let startTime = Timeline.time();
            let passedTime = startTime - lastTime;
            lastTime = startTime;

            unprocessedTime += (passedTime / Timeline.SECOND);
            frameCounter += passedTime;

            while (unprocessedTime > frameRate) {

                render = true;
                unprocessedTime -= frameRate;

                if (!this.isRunning || !document.hasFocus()) {
                    window.clearInterval(interID);
                }

                this.timeline.delta = frameRate;

                this.display.clear();
                this.timeline.update();
                this.scene.update(this.timeline);
                this.display.swapBuffers();
                if (frameCounter >= Timeline.SECOND) {
                    fps = frames;
                    frames = 0;
                    frameCounter = 0;
                }
                this.display.paintFPS(fps);
            }

            if (render) {
                frames++;
            }
        }

        let interID = window.setInterval(loop, 1);
        this.callback({ action: GizmoSoft.STATUS.RUNNING });
    }

    pause() {
        this.isRunning = false;
        this.callback({ action: GizmoSoft.STATUS.PAUSED });
    }
}
GizmoSoft.STATUS = {
    READY: 1,
    RUNNNING: 2,
    PAUSED: 3
}
GizmoSoft.CANVAS_SIZE = {
    w: 320,
    h: 240
}
GizmoSoft.FPS = 80;

export default GizmoSoft;