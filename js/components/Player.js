class Player {

    constructor() {

        this.currentSpeed = 0;
        this.currentTurnSpeed = 0;

    }

    move(input) {

        this.checkInputs(input);

    }

    updateInput(input, delta) {

		// Speed and rotation amounts are hardcoded here.
		// In a more general system, you might want to have them as variables.
        //floats
		const sensitivityX = 1 * delta;
		const sensitivityY = 0.8 * delta;
		const movAmt = 15.0 * delta;

		if(input.getKey(Input.Key.W)) {
            this.currentSpeed = Player.ACCELERATION_SPEED;
        } else if(input.getKey(Input.Key.S)) {
            this.currentSpeed = -Player.ACCELERATION_SPEED;
        } else {
            this.currentSpeed = 0;
        }

		if(input.getKey(Input.Key.D)) {
            this.currentTurnSpeed = -Player.TURN_SPEED;
        } else if(input.getKey(Input.Key.A)){
            this.currentSpeed = Player.TURN_SPEED;
        } else {
            this.currentTurnSpeed = 0;
        }

		// if(input.getKey(Input.Key.UP)) this.rotate(this.transform.rotation.getRight(), sensitivityY);
		// else if(input.getKey(Input.Key.DOWN)) this.rotate(this.transform.rotation.getRight(), -sensitivityY);
    }
}

Player.ACCELERATION_SPEED = 20;
Player.TURN_SPEED = 160;

export default Player;