import Vec4 from "../math/Vec4.js";
import IndexedModel from "./IndexedModel.js";

class OBJModel {

    constructor() {

		this.vIndexOffset = 0;
		this.tIndexOffset = 0;
		this.nIndexOffset = 0;

		this.reset();
        return this;
	}

	reset() {

        this.positions = [];
        this.texCoords = [];
        this.normals = [];
        this.indices = [];
		this.meshName = "";
		this.materialPath = "";
        this.hasTexCoords = false;
		this.hasNormals = false;
	}

    loadFile(name, fileName, callback) {

        this.name = name;
        this.callback = callback;

        fetch(fileName).
        then(res => {
            return res.text();
        }).
        then(tRes => {
            this.parse(tRes)
        });
	}
	
	/**
	 * @param {Object} data 
	 * Must cointain vertices, uvs, indices and normals arrays
	 */
	generate(data) {

		let objString = "";

        this.name = data.name;

		for (let i = 0; i < data.vertices.length; i+=3) {
			objString += "v " + data.vertices[i] + " " + data.vertices[i+1] + " " + data.vertices[i+2] + "\n";
		}
		for (let i = 0; i < data.texCoords.length; i+=2) {
			objString += "vt " + data.texCoords[i] + " " + data.texCoords[i+1] + "\n";
		}
		for (let i = 0; i < data.normals.length; i+=3) {
			objString += "vn " + data.normals[i] + " " + data.normals[i+1] + " " + data.normals[i+2] + "\n";
		}
		objString += "g terrain\n";
		for (let i = 0; i < data.indices.length; i+=3) {
			objString += 	"f " + (data.indices[i] + 1) + "/" + (data.indices[i] + 1) + "/" + (data.indices[i] + 1) + " "
							+ (data.indices[i+1] + 1) + "/" + (data.indices[i+1] + 1) + "/" + (data.indices[i+1] + 1) + " "
							+ (data.indices[i+2] + 1) + "/" + (data.indices[i+2] + 1) + "/" + (data.indices[i+2] + 1) + "\n";
		}

		return this.parse(objString);
	}

    parse(data) {

		const lines = data.split("\n");
		
		let isBuilding = false;
		let tokens;
		let tokenType;

        const objects = [];
        for(let i = 0; i < lines.length; i++) {

            tokens = lines[i].split(" ");
			tokens = OBJModel.removeEmptyStrings(tokens);
			tokenType = tokens[0];

			if(isBuilding && (tokenType !== "f" && tokenType !== "usemtl")) {

				objects.push(this.toIndexedModel())

				this.vIndexOffset += this.positions.length;
				this.tIndexOffset += this.texCoords.length;
				this.nIndexOffset += this.normals.length;

				isBuilding = false;
				this.reset();
			}

            if(tokens.length === 0 || tokenType === "#") {
                continue;
			}
			else if (tokenType === "mtllib") {
				this.materialPath = tokens[1];
			}
            else if(tokenType === "v") {
                this.positions.push(new Vec4(parseFloat(tokens[1]),
                                             parseFloat(tokens[2]),
                                             parseFloat(tokens[3]),1));
            } else if(tokenType === "vt") {
                this.texCoords.push(new Vec4(parseFloat(tokens[1]),
						                    1.0 -  parseFloat(tokens[2]),0,0));
                                             
            } else if(tokenType === "vn") {
				this.normals.push(new Vec4( parseFloat(tokens[1]),
                                            parseFloat(tokens[2]),
                                            parseFloat(tokens[3]),0));
			} else if(tokenType === "f") {

				for(let j = 0; j < tokens.length - 3; j++) {

					this.indices.push(this.parseOBJIndex(tokens[1]));
					this.indices.push(this.parseOBJIndex(tokens[2 + j]));
					this.indices.push(this.parseOBJIndex(tokens[3 + j]));
				}
			} else if(tokenType === "g") {
				isBuilding = true;
				this.meshName = lines[i].replace("g ", "");
			}
		}

		// if(this.meshName === "terrain") console.log("objString");

		if(this.callback) this.callback(objects);
		else return objects[0];
    }

    toIndexedModel() {

        const result = new IndexedModel(this.meshName);
		const normalModel = new IndexedModel();
		const resultIndexMap = [];
		const normalIndexMap = [];
		const indexMap = [];

		
		for(let i = 0; i < this.indices.length; i++) {

			const currentIndex = this.indices[i];

			const currentPosition = this.positions[currentIndex.getVertexIndex()];
			let currentTexCoord;
			let currentNormal;

			if(this.hasTexCoords)
				currentTexCoord = this.texCoords[currentIndex.getTexCoordIndex()];
			else
				currentTexCoord = new Vec4(0,0,0,0);

			if(this.hasNormals)
				currentNormal = this.normals[currentIndex.getNormalIndex()];
			else
				currentNormal = new Vec4(0,0,0,0);

			let modelVertexIndex = resultIndexMap[currentIndex];

			if(!modelVertexIndex) {

				modelVertexIndex = result.getPositions().length;
				resultIndexMap.push({
                    currentIndex: currentIndex, 
                    modelVertexIndex: modelVertexIndex });

				result.getPositions().push(currentPosition);
				result.getTexCoords().push(currentTexCoord);
				if(this.hasNormals)
					result.getNormals().push(currentNormal);
			}

			let normalModelIndex = normalIndexMap[currentIndex.getVertexIndex()];

			if(normalModelIndex === undefined) {
				normalModelIndex = normalModel.getPositions().length;
				normalIndexMap.push({
                        currentIndex: currentIndex.getVertexIndex(), 
                        normalModelIndex: normalModelIndex
                    });

				normalModel.getPositions().push(currentPosition);
				normalModel.getTexCoords().push(currentTexCoord);
				normalModel.getNormals().push(currentNormal);
				normalModel.getTangents().push(new Vec4(0,0,0,0));
			}

			result.getIndices().push(modelVertexIndex);
			normalModel.getIndices().push(normalModelIndex);
			indexMap.push({
                modelVertexIndex: modelVertexIndex, 
                normalModelIndex: normalModelIndex
            });
		}

		if(!this.hasNormals)
		{
			normalModel.calcNormals();

			for(let i = 0; i < result.getPositions().length; i++)
				result.getNormals().push(normalModel.getNormals()[indexMap[i]]);
		}

		normalModel.calcTangents();

		for(let i = 0; i < result.getPositions().length; i++)
			result.getTangents().push(normalModel.getTangents()[indexMap[i]]);

		return result;
	}

    parseOBJIndex(token) {


		const values = token.split("/");
		const result = new OBJIndex();

		result.setVertexIndex((parseInt(values[0]) - 1 - (this.vIndexOffset)));

		if(values.length > 1) {
			if(values[1] !== "") {
				this.hasTexCoords = true;
				result.setTexCoordIndex(parseInt(values[1]) - 1 - (this.tIndexOffset));
			}

			if(values.length > 2) {
				this.hasNormals = true;
				result.setNormalIndex(parseInt(values[2]) - 1 - (this.nIndexOffset));
			}
		}

		return result;
	}

    static removeEmptyStrings(data) {
        
        const result = [];
		
		for(let i = 0; i < data.length; i++)
			if(data[i] !== "")
				result.push(data[i]);
		
		return result;
	}
}

class OBJIndex {

    constructor() {
        this.vertextIndex;
        this.texCoordIndex;
		this.normalIndex;
    }

    equals(obj) {
        return  this.vertextIndex === obj.vertextIndex &&
                this.texCoordIndex === obj.texCoordIndex &&
                this.normalIndex === obj.normalIndex;
    }

    hashCode() {
        const BASE = 17;
        const MULTIPLIER = 31;

        const result = BASE;

        result = MULTIPLIER * result + this.vertextIndex;
        result = MULTIPLIER * result + this.texCoordIndex;
        result = MULTIPLIER * result + this.normalIndex;

        return result;
    }

    // GETTERS SETTERS
    
    getVertexIndex() { return this.vertextIndex; }
    getTexCoordIndex() { return this.texCoordIndex; }
    getNormalIndex() { return this.normalIndex; }
    setVertexIndex(val) { this.vertextIndex = val; }
    setTexCoordIndex(val) { this.texCoordIndex = val; }
    setNormalIndex(val) { this.normalIndex = val; }
}

export default OBJModel;