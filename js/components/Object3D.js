import Transform from "./Transform.js";
import Vec4 from "../math/Vec4.js";
import Quaternion from "../math/Quaternion.js";

class Object3D {

    constructor(data, useLight=true) {

        this.mesh = data.mesh;
        this.objects = data.objects ? data.objects : [];
        this.name = data.name;

        this.texture = data.texture;
        if(data.rad !== undefined) {
            this.texture.radiance = data.rad;
        }
        else {
            this.texture.radiance = 1;
        }

        this.visible = true;
        this.texture.useLight(useLight);

        this.reset();
    }

    //params: Display, Mat4
    update(renderer, viewPerspective) {

        if(!this.visible) return;

        if(this.mesh) this.mesh.draw(   renderer,
                                        viewPerspective,
                                        this.transform.getTransformation(),
                                        this.texture );

        for(let i = 0; i < this.objects.length; i++) {
            this.objects[i].update(renderer, viewPerspective);
        }
        // this.reset();
    }

    addObjects(obj3d) {
        this.objects.push(obj3d);
    }
    getObject(objName) {
        for (let i = 0; i < this.objects.length; i++) {
            if(this.objects[i].name === objName) return this.objects[i];
        }
    }
    removeObject(objName) {
        const tmp = [];
        for(let i = 0; i < this.objects.length; i++) {
            if(this.objects[i].name !== objName) tmp.push(this.objects[i]);
        }
        this.objects = tmp;
    }

    reset() {
        this.transform = new Transform(new Vec4(0,0,0), new Quaternion(), new Vec4(1,1,1));
        for(let i = 0; i < this.objects.length; i++) {
            this.objects[i].reset();
        }
    }

    setMainTexture(texID) {

        this.texture.setMainTexture(texID);
    }

    //params: Vec4
    setPos(pos) {
        // this.transform = this.transform.setPos(pos);
        this.transform.position = pos;

        const p = this.transform.position;
        for (let i = 0; i < this.objects.length; i++) {
            this.objects[i].position = p;
        }
    }
    getPos() {

        return this.transform.position;
    }

    //params: Quaternion
    setRotate(rot) {
        // this.transform = this.transform.rotate(rot);
        this.transform.rotate(rot);
        const r = this.transform.rotation;
        for (let i = 0; i < this.objects.length; i++) {
            // this.objects[i].setRotate(r);
            this.objects[i].rotate(r);
        }
    }

    /**
     * @returns {Quaternion}
     */
    getRot() {
        return this.transform.getRot();
    }

    //params: Vec4
    setScale(scale) {
        this.transform = this.transform.setScale(scale);
    }

    // get name() {
    //     return this.mesh.name;
    // }

    setRadiance(rad) {
        this.texture.bmp.setRadiance(rad);
    }

    //params: Vec4, Vec4
    lookAt(dir, up) {
        this.transform = this.transform.lookAt(dir, up);
    }

    clone() {
        return new Object3D({
            mesh: this.mesh,
            rad: this.texture.getRadiance(),
            name: this.name,
            texture: this.texture,
            objects: this.objects
        });
    }
}

export default Object3D;