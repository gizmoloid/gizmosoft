import Vec4 from "../math/Vec4.js";

class Vertex {

    //Vec4, Vec4, Vec4
    constructor(pos, textCoords, normal) {

        this.pos = pos;
        this.textCoords = textCoords;

        if(!textCoords) {
            this.textCoords = pos;
        } else {
            this.textCoords = textCoords;
        }
        if(!normal) {
            this.normal = pos;
        } else {
            this.normal = normal;
        }
    }

    clone() {
        return new Vertex(this.pos, this.textCoords, this.normal);
    }

    //param: Mat4, Mat4 returns: Vertex
    transform(transform, normalTransform) {

        return new Vertex(transform.transform(this.pos), this.textCoords, normalTransform.transform(this.normal));
    }

    perspectiveDivide() {

        const v = new Vec4(   this.pos.x / this.pos.w,
                            this.pos.y / this.pos.w,
                            this.pos.z / this.pos.w,
                            this.pos.w );
        
        return new Vertex(v, this.textCoords, this.normal);
    }

    isInsideViewFrustum() {

        return  Math.abs(this.pos.x) <= Math.abs(this.pos.w) &&
                Math.abs(this.pos.y) <= Math.abs(this.pos.w) &&
                Math.abs(this.pos.z) <= Math.abs(this.pos.w)
    }

    triangleAreaTimesTwo(b, c) {

        const x1 = b.pos.x - this.pos.x;
        const y1 = b.pos.y - this.pos.y;

        const x2 = c.pos.x - this.pos.x;
        const y2 = c.pos.y - this.pos.y;

        return (x1 * y2 - x2 * y1)
    }

    //params: Vec4, float returns: Vertex
    lerp(other, lerpAmt) {
        return new Vertex(
            Vec4.lerp(this.pos, other.pos, lerpAmt),
            Vec4.lerp(this.textCoords, other.textCoords, lerpAmt),
            Vec4.lerp(this.normal, other.normal, lerpAmt));
    }

    get(i) {
        switch(i) {
            case 0:
                return this.pos.x;
            case 1:
                return this.pos.y;
            case 2:
                return this.pos.z;
            case 3:
                return this.pos.w;
        }
    }

    // GETTERS SETTERS
    getPosition() { return this.pos; }
    setPosition(pos) { this.pos = pos; }
    getTexCoords() { return this.textCoords; }
    getNormal() { return this.normal; }
}
export default Vertex;