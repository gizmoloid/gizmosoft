import Object3D from "./Object3D.js";
import Input from "../core/Input.js";
import Vec4 from "../math/Vec4.js";
import Quaternion from "../math/Quaternion.js";

class Car extends Object3D {
    constructor(resources) {

        const model = resources.getObject3D("car");

        super({
            mesh: null,
            objects: model.objects,
            name: "car",
            texture: model.texture
        })

        this.chasi = this.getObject("chassi");


        // console.log(this.chasi)
    }

    updateInput(input, delta) {

		// Speed and rotation amounts are hardcoded here.
		// In a more general system, you might want to have them as variables.
        //floats
		const sensitivityX = 1 * delta;
		const sensitivityY = 0.8 * delta;
		const movAmt = 15.0 * delta;

		if(input.getKey(Input.Key.W)) this.move(this.transform.rotation.getForward(), movAmt);
		else if(input.getKey(Input.Key.S)) this.move(this.transform.rotation.getForward(), -movAmt);
		// if(input.getKey(Input.Key.W)) this.move(this.transform.getRot().getForward(), movAmt);
		// else if(input.getKey(Input.Key.S)) this.move(this.transform.getRot().getForward(), -movAmt);

		if(input.getKey(Input.Key.A)) this.rotate(Car.Y_AXIS, -sensitivityX);
		else if(input.getKey(Input.Key.D)) this.rotate(Car.Y_AXIS, sensitivityX);


		// if(input.getKey(Input.Key.UP)) this.rotate(this.transform.getRot().getRight(), sensitivityY);
		// else if(input.getKey(Input.Key.DOWN)) this.rotate(this.transform.getRot().getRight(), -sensitivityY);
		if(input.getKey(Input.Key.UP)) this.rotate(this.transform.rotation.getRight(), sensitivityY);
		else if(input.getKey(Input.Key.DOWN)) this.rotate(this.transform.rotation.getRight(), -sensitivityY);
    }

	move(dir, amt) {
		const pos = this.transform.position.addVec(dir.multiply(amt));
		// this.transform = this.transform.setPos(pos);
		this.transform.position = pos;
		
		for (let i = 0; i < this.objects.length; i++) {
			this.objects[i].setPos( pos );
		}
        
    }
    
    //Vec4, float
	rotate(axis, angle) {
		const rot = new Quaternion({
			type: Quaternion.Type.INIT_VEC_ANGLE,
			vec: axis,
			angle: angle
		})
		// this.transform = this.transform.rotate(rot);
		this.transform.rotate(rot);
		
		for (let i = 0; i < this.objects.length; i++) {
			// this.objects[i].setRotate( rot );
			this.objects[i].transform.rotate( rot );
		}
	}
}
Car.Y_AXIS = new Vec4(0, 1, 0);

export default Car