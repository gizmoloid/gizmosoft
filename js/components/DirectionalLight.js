import Vec4 from "../math/Vec4.js";
import Color from "../math/Color.js";

class DirectionalLight {
    constructor(dir, intensity, color) {
        this.dir = dir ? dir : new Vec4(0,1,-1);
        this.intensity = intensity ? intensity : 1;
        this.color = color ? color : new Color(1,0,0,1);
    }
}

export default DirectionalLight;