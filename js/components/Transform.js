import Vec4 from "../math/Vec4.js";
import Quaternion from "../math/Quaternion.js";
import Mat4 from "../math/Mat4.js";

class Transform {

	/**
	 * @param {Vec4} pos 
	 * @param {Quaternion} rot 
	 * @param {Vec4} scale 
	 */
    constructor(pos, rot, scale) {

		this.position = pos;
        this.rotation = rot;
		this.scale = scale;

		if(this.position === undefined) this.position = new Vec4(0, 0, 0, 0);
		if(this.rotation === undefined) this.rotation = new Quaternion();
		if(this.scale === undefined) this.scale = new Vec4(1, 1, 1, 1);
	}
	
	clone() {

		return new Transform(this.position, this.rotation, this.scale);
	}

	/**
	 * @param {Vec4} pos 
	 * @returns {Transform}
	 */
	// setPos(pos) {
	// 	return new Transform(pos, this.rotation, this.scale);
	// }

	/**
	 * @param {Quaternion} rotation 
	 * @returns {Transform}
	 */
	rotate(rot) {
		this.rotation = rot.multiplyQuat(this.rotation).normalized();
		// return new Transform(this.position, rot.multiplyQuat(this.rotation).normalized(), this.scale);
	}

	/**
	 * 
	 * @param {Quaternion} rot 
	 */
	setRot(rot) {
		this.rotation = rot;
	}

	/**
	 * @param {Vec4} scl 
	 * @returns {Transform}
	 */
	setScale(scl) {
		return new Transform(this.position, this.rotation, scl);
	}

	/**
	 * @param {Vec4} point 
	 * @param {Vec4} up 
	 * @returns {Transform}
	 */
	lookAt(point, up) {

		// console.log(this.getLookAtRotation(point, up));
		// return this.rotate(this.getLookAtRotation(point, up));
		this.rotate(this.getLookAtRotation(point, up));
	}

	//Vec4, Vec4 / returns: Quaternion
	/**
	 * 
	 * @param {Vec4} point 
	 * @param {Vec4} up 
	 * @returns {Quaternion}
	 */
	getLookAtRotation(point, up) {

		const mat = new Mat4().rotationFU(point.subtractVec(this.position).normalized(), up);
		
		return new Quaternion({
            type: Quaternion.Type.INIT_MAT,
			mat: mat
		});
	}


    //Mat4
	getTransformation() {

		const translationMatrix = new Mat4().translation(this.position.getX(), this.position.getY(), this.position.getZ());
		const rotationMatrix = this.rotation.toRotationMatrix();
		
		const scaleMatrix = new Mat4().scale(this.scale.getX(), this.scale.getY(), this.scale.getZ());

		return translationMatrix.multiply(rotationMatrix.multiply(scaleMatrix));
	}

	getTransformedPos() {
		return this.position;
	}

	getTransformedRot() {
		return this.rotation;
	}

	// getPos() {
	// 	return this.position;
	// }

	/**
	 * @returns {Quaternion}
	 */
	// getRot() {
	// 	return this.rotation;
	// }

	// getScale() {
	// 	return this.scale;
	// }
}

export default Transform;