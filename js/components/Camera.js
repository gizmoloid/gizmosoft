import Vec4 from "../math/Vec4.js";
import Transform from "./Transform.js";
import Mat4 from "../math/Mat4.js";
import Quaternion from "../math/Quaternion.js";
import Utils from "../utils/Utils.js";
import Input from "../core/Input.js";

class Camera {

	//params: Mat4
    constructor(target) {

		//Transform
		this.perspective = parseFloat(Utils.toRadians(70.0));
		this.ratio = parseFloat(target.width) / parseFloat(target.height);
		this.zNear = 0.1;
		this.zFar = 1000.0;

        this.transform = new Transform();
		this.projection = new Mat4().perspective( this.perspective, this.ratio, this.zNear, this.zFar );
		
		this.offsetPosition = new Vec4();

		this.distance = 1;
		this.cameraSpeed = 8;
	}

    resetTarget(target) {

		this.ratio = parseFloat(target.width) / parseFloat(target.height);
        this.projection = new Mat4().perspective( this.perspective, this.ratio, this.zNear, this.zFar );
	}
	
	reset() {
		this.transform = new Transform();
	}

	//PARAMS: float
	setPerspective(angle) {

		this.perspective = parseFloat(Utils.toRadians(angle));
        this.projection = new Mat4().perspective( this.perspective, this.ratio, this.zNear, this.zFar );
	}

    getViewProjection() {

        //Mat4
		const cameraRotation = this.transform.getTransformedRot().conjugate().toRotationMatrix();

        //Vec4
		const cameraPos = this.transform.getTransformedPos().multiply(-1);

        //Mat4
		const cameraTranslation = new Mat4().translation(cameraPos.x, cameraPos.y, cameraPos.z);

		return this.projection.multiply(cameraRotation.multiply(cameraTranslation));
	}

	/**
	 * @param {Vec4} dir 
	 * @param {Number} amt 
	 */
	move(dir, amt) {
		this.transform = this.transform.setPos(this.transform.getPos().addVec(dir.multiply(amt)));
	}

	moveTo(to, delta) {
		this.transform.position = Vec4.lerp(this.transform.position, to.addVec(this.offsetPosition), delta)
	}

	translate(dir, p){
		this.transform.position = this.transform.position.addVec(dir.multiplyVec(p));
	}

    setPositionOffset(offset) {
        this.offsetPosition = offset;
    }

	/**
	 * 
	 * @param {Object3D} object 
	 */
	follow(object, delta) {

		this.transform.position = object.transform.position;
		this.transform.rotation = object.transform.rotation;

		const dir = this.transform.rotation.getForward();
		this.transform.position = this.transform.position.addVec(dir.multiply(-1));
		this.transform.position = this.transform.position.addVec(new Vec4(0,0.5,0));

	}

	/**
	 * @param {Vec4} point 
	 * @param {Vec4} up 
	 */
	lookAt(point, up) {
		this.transform.lookAt(point, up);
	}
}

Camera.Y_AXIS = new Vec4(0, 1, 0);

export default Camera;