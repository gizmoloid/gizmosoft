import Vertex from "./Vertex.js";

class Mesh {

    constructor(data) {

        if(!data) return;

        this.meshData = data;
        this.vertices = [];
        this.name = data.name;

        for(let i = 0; i < data.getPositions().length; i++) {

			this.vertices.push(new Vertex(
                data.getPositions()[i],
                data.getTexCoords()[i],
                data.getNormals()[i]));
		}
        this.indices = data.getIndices();
    }

    //context, Mat4, Mat4, Bitmap
    draw(context, viewProjection, transform, texture) {

        const mvp = viewProjection.multiply(transform);

        let l = this.indices.length-1;

        
        for(let i = 0; i < this.indices.length; i += 3) {
            if(this.name === "terrain") {

               
                // console.log(this.vertices[this.indices[i]])
                // console.log("index", i)
                // console.log(this.vertices[this.indices[i + 1]])
                // console.log("index", i + 1)
                // console.log(this.vertices[this.indices[i + 2]])
                // console.log("index", i + 2)
                // console.log("-----")
            }
            context.drawTriangle(
					this.vertices[this.indices[i]].transform(mvp, transform),
					this.vertices[this.indices[i + 1]].transform(mvp, transform),
					this.vertices[this.indices[i + 2]].transform(mvp, transform),
                    texture.bmp);
        }
    }

    //GETTERS SETTERS
    getVertex(i) {return this.vertices[i]; }
    getIndex(i) {return this.indices[i]; }
    getNumIndices() {return this.indices.length; }
    clone() { return new Mesh(this.meshData); }
}

// 1922

export default Mesh;