import Vec4 from "../math/Vec4.js";

class Particle {

    constructor() {

        this.position = new Vec4();
        this.velocity = new Vec4(1);
        this.acceleration = new Vec4();
        this.forceAccum = new Vec4();

        this.damping = 0.6;
        this.inverseMass = 2;
    }

    integrate(duration) {

        if (this.inverseMass <= 0.0) return;


        this.position.addScaledVector(this.velocity, duration);

        const resAcc = this.acceleration;
        resAcc.addScaledVector(this.forceAccum, this.inverseMass);

        this.velocity.addScaledVector(resAcc, duration);

        this.velocity.timesEqualsNum(Math.pow(this.damping, duration));

        this.clearAccumulator();
    }
    
    setMass(mass) {
        this.inverseMass = 1.0 / mass;
    }
    
    getMass() {
        if (this.inverseMass == 0) {
            return REAL_MAX;
        } else {
            return 1.0 / this.inverseMass;
        }
    }
    
    hasFiniteMass() {
        return this.inverseMass >= 0.0;
    }
    
    clearAccumulator() {
        this.forceAccum.clear();
    }
    
    addForce(force) {
        this.forceAccum.plusEquals(force);
    }
}

export default Particle;