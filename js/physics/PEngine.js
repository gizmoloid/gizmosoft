import Particle from "./Particle.js";

class PEngine {

    constructor(cb) {

        this.isRunning = false;
        this.callback = cb;
        this.intervalID = null;
        console.log("hello physics");

        this.particle = new Particle();
        
    }

    run() {

        console.log("start run");
        let counter = 0;
        this.isRunning = true;
        const loop = () => {


            counter++;
            this.particle.integrate(counter);

            if(!this.isRunning) {
                clearInterval(this.intervalID);
                this.intervalID = null;
            }
        }


        this.callback();

        this.intervalID = setInterval(loop, 1);
    }

    pause() {
        this.isRunning = false;
    }
}

export default PEngine;