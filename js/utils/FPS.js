import Timeline from "../core/Timeline.js";

class FPS {

    constructor() {

        this.previousTime = Timeline.time();
        this.currentFPS = 0;
    }

    update(tick) {

        var now = Timeline.time();
        var currentFPS = 1000.0 * (now - this.previousTime);

        this.previousTime = now;
        console.log(currentFPS);
    }
}

// class FPS {

//     constructor() {
//         this.previousTime = Timeline.time();
//         this.currentFPS = 0;
//         this.label = document.createElement("label");
//         document.body.appendChild(this.label);
//     }

//     update() {
//         const now = Timeline.time();
//         this.label.innerHTML = "FPS: " + Math.round(1000.0 * (now - this.previousTime));
//         this.previousTime = now;
//     }
// }

export default FPS;