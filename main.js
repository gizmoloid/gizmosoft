import GizmoSoft from "./js/GizmoSoft.js";

let gzm;
let phy;
function init() {
    gzm = new GizmoSoft((status) => {
        switch(status) {
            case GizmoSoft.STATUS.READY:
            setupUI();
            break;
        }
    });
    // initPhysics();
}

function initPhysics() {
    phy = new Worker("physics.js", {type: "module"});
    phy.addEventListener("message", (e) => {

        // console.log(e);
    })
    
}

//UI STUFF
function setupUI() {


    if(window.innerWidth > GizmoSoft.CANVAS_SIZE.w * 2) {
        canvas.style.transform = "scale(2)";
    } else {
        canvas.style.transform = "scale(1.5)";
    }

    window.addEventListener("blur", () => {
        if (gzm.isRunning) {
            playstop.click(); 
        }
    });

    //UI EVENTS
    playstop.onclick = (e) => {
        if (gzm.isRunning) {
            gzm.pause();
            // phy.postMessage({ action: "PAUSE" })
            e.currentTarget.innerHTML = "PLAY";
        } else {
            gzm.run();
            // phy.postMessage({ action: "RUN" })
            e.currentTarget.innerHTML = "STOP";
        }
    }
}

window.onload = init;
// window.onload = testWorker;